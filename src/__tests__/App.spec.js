import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import App from '../App'

describe('App.spec.js', () => {
  test('Deve exibir primeiro a tela de favorecidos', () => {
    const { getAllByText } = render(<App />)

    expect(getAllByText('Seus favorecidos').length).toBe(2)
  })

  test('Deve abrir form de cadastro de favorecido ao clicar em cadastrar', () => {
    const { getByText, queryByText } = render(<App />)

    fireEvent.click(screen.getByTestId('botao-cadastro-favorecido'))

    expect(getByText('Quais os dados do favorecido?')).toBeInTheDocument()
    expect(queryByText('Seus favorecidos')).not.toBeInTheDocument()
  })
})
