import React, { useState, useEffect } from 'react'
import cx from 'classnames'
import { Layout, Button, Input, message, Modal, Typography, Row, Col } from 'antd'
import { PlusOutlined, CloseOutlined, ExclamationCircleOutlined } from '@ant-design/icons'

// Precisei desativar a localizacao porque estava quebrando os testes, alguns recursos da lib vão ficar em ingles
//import ptBR from 'antd/es/locale/pt_BR'

import {
  getFavorecidos,
  cadastrarFavorecido,
  deletarFavorecidos,
  editarFavorecido,
} from './gateways/favorecidosGateway'

import FormFavorecido from './componentes/FormFavorecido'
import TabelaFavorecidos from './componentes/TabelaFavorecidos'
import TagStatus from './componentes/TagStatus'

import 'antd/dist/antd.css'
import './App.css'

const { Header, Content, Footer } = Layout
const { confirm } = Modal

function getConteudoModalDeletarRegistros(registrosSelecionados, favorecidos) {
  const registrosParaDeletar = registrosSelecionados.length
  let nomeFavorecido = ''

  if (registrosParaDeletar === 1) {
    nomeFavorecido = favorecidos.filter(
      (favorecido) => favorecido['_id'] === registrosSelecionados[0]
    )[0].nome
  }

  return (
    <>
      <Typography.Title level={4}>
        Você confirma a exclusão{' '}
        {registrosParaDeletar > 1
          ? `de ${registrosParaDeletar} favorecidos`
          : `do favorecido ${nomeFavorecido}`}
        ?
      </Typography.Title>
      <Typography.Paragraph>
        O histórico de pagamentos para{' '}
        {registrosParaDeletar > 1
          ? 'estes favorecidos serão mantidos, mas eles serão removidos'
          : 'este favorecido será mantido, mas ele será removido'}{' '}
        da sua lista de favorecidos.
      </Typography.Paragraph>
    </>
  )
}

function App() {
  const [carregando, setCarregando] = useState(true)
  const [favorecidos, setFavorecidos] = useState([])
  const [registrosSelecionados, setRegistrosSelecionados] = useState([])
  const [cadastrandoFavorecido, setCadastrandoFavorecido] = useState(false)
  const [idFavorecidoEditar, setIdFavorecidoEditar] = useState(undefined)
  const [termoPesquisa, setTermoPesquisa] = useState(undefined)

  async function getTodosFavorecidos() {
    try {
      setCarregando(true)

      const { data } = await getFavorecidos()

      setFavorecidos(data)
      setCarregando(false)
    } catch (error) {
      console.log(error)
      // TODO: Handle error
    }
  }

  useEffect(() => {
    getTodosFavorecidos()
  }, [])

  const onChangeLinhasSelecionadas = (chavesLinhasSelecionadas) => {
    setRegistrosSelecionados(chavesLinhasSelecionadas)
  }

  const selecaoLinhas = {
    selectedRowKeys: registrosSelecionados,
    onChange: onChangeLinhasSelecionadas,
  }

  const onClickFecharCadastro = () => setCadastrandoFavorecido(false)

  async function onSubmitFavorecido(dados) {
    try {
      await cadastrarFavorecido(dados)
      setCadastrandoFavorecido(false)
      getTodosFavorecidos()
      message.success('Favorecido cadastrado com sucesso')
    } catch (error) {
      message.error('Erro ao cadastrar o favorecido')
    }
  }

  async function onEditarFavorecido(dados) {
    try {
      await editarFavorecido(idFavorecidoEditar, dados)
      setIdFavorecidoEditar(undefined)
      getTodosFavorecidos()
      message.success('Favorecido alterado com sucesso')
    } catch (error) {
      message.error('Erro ao editar o favorecido')
    }
  }

  function abrirModalExclusao(idsExcluir, callbackExcluido) {
    const registrosParaDeletar = idsExcluir.length
    const titulo =
      registrosParaDeletar > 1
        ? `Excluir ${registrosParaDeletar} favorecidos`
        : 'Excluir favorecido'

    confirm({
      title: titulo,
      icon: <ExclamationCircleOutlined />,
      content: getConteudoModalDeletarRegistros(idsExcluir, favorecidos),
      okText: 'Confirmar exclusão',
      okType: 'danger',
      cancelText: 'Cancelar',
      async onOk() {
        try {
          await deletarFavorecidos(idsExcluir)
          callbackExcluido()
          message.success('Exclusão realizada com sucesso')
        } catch (erro) {
          message.error('Falha na exclusão, tente novamente mais tarde')
        }
      },
    })
  }

  function onClickExcluirSelecionados() {
    abrirModalExclusao(registrosSelecionados, () => {
      console.log('apagou registros')
      setRegistrosSelecionados([])
      getTodosFavorecidos()
    })
  }

  function onClickExcluirFavorecido(idFavorecido) {
    abrirModalExclusao([idFavorecidoEditar], () => {
      setIdFavorecidoEditar(undefined)
      getTodosFavorecidos()
    })
  }

  function onClickFavorecido(idFavorecido) {
    setIdFavorecidoEditar(idFavorecido)
  }

  const getFavorecidoEditar = () =>
    favorecidos.filter((favorecido) => favorecido['_id'] === idFavorecidoEditar)[0]

  const onClickFecharModal = () => setIdFavorecidoEditar(undefined)

  function getFavorecidosFiltrados() {
    if (termoPesquisa !== undefined) {
      const lowerCaseTermoPesquisa = termoPesquisa.toLowerCase()
      
      return favorecidos.filter(
        (favorecido) =>
          favorecido.nome.toLowerCase().includes(lowerCaseTermoPesquisa) ||
          favorecido.documento.toLowerCase().includes(lowerCaseTermoPesquisa) ||
          favorecido.agencia.toLowerCase().includes(lowerCaseTermoPesquisa) ||
          favorecido.conta.toLowerCase().includes(lowerCaseTermoPesquisa)
      )
    }

    return favorecidos
  }

  return (
    <Layout style={{ height: '100vh' }}>
      <Header />
      <Content>
        <div
          className={cx('navbar', {
            'navbar-cadastrando-favorecido': cadastrandoFavorecido,
          })}
        >
          {cadastrandoFavorecido ? (
            <Button
              size="large"
              type="link"
              ghost
              icon={<CloseOutlined />}
              onClick={() => setCadastrandoFavorecido(false)}
            />
          ) : (
            <div className="navbar-tab-item">
              <div className="navbar-tab-item-text">Seus favorecidos</div>
            </div>
          )}
        </div>
        {!cadastrandoFavorecido && (
          <div className="conteudo-header">
            <Row justify="space-space-between" align="middle">
              <Col flex={1}>
                <div className="conteudo-header-containerFavorecidos">
                  <span className="conteudo-header-titulo">Seus favorecidos</span>
                  <Button
                    type="primary"
                    size="large"
                    shape="circle"
                    icon={<PlusOutlined />}
                    onClick={() => setCadastrandoFavorecido(true)}
                    data-testid="botao-cadastro-favorecido"
                  />
                </div>
              </Col>
              <Col>
                <Input.Search
                  placeholder="Nome, CPF, agência ou conta"
                  size="large"
                  style={{ width: '300px' }}
                  onSearch={(valor) => setTermoPesquisa(valor)}
                />
              </Col>
            </Row>
          </div>
        )}

        <div className="conteudo-pagina">
          {cadastrandoFavorecido ? (
            <FormFavorecido onSubmit={onSubmitFavorecido} onClickFechar={onClickFecharCadastro} />
          ) : (
            <>
              <Button
                type="primary"
                size="large"
                danger
                style={{ marginBottom: '20px' }}
                onClick={onClickExcluirSelecionados}
                disabled={registrosSelecionados.length === 0}
              >
                Excluir selecionados
              </Button>
              <TabelaFavorecidos
                carregando={carregando}
                favorecidos={getFavorecidosFiltrados()}
                selecaoLinhas={selecaoLinhas}
                onClickFavorecido={onClickFavorecido}
              />
            </>
          )}
        </div>
      </Content>
      <Footer />
      {idFavorecidoEditar !== undefined && (
        <Modal
          visible
          title={getFavorecidoEditar(idFavorecidoEditar).nome}
          footer={null}
          onCancel={() => setIdFavorecidoEditar(undefined)}
        >
          <TagStatus status={getFavorecidoEditar(idFavorecidoEditar).status} />
          <FormFavorecido
            favorecidoEditar={getFavorecidoEditar(idFavorecidoEditar)}
            onSubmit={onEditarFavorecido}
            onClickFechar={onClickFecharModal}
            onClickDelete={() => onClickExcluirFavorecido(idFavorecidoEditar)}
          />
        </Modal>
      )}
    </Layout>
  )
}

export default App
