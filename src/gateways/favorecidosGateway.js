import axios from 'axios'

const urlBase = process.env.REACT_APP_API_URL || 'http://localhost:5000/api'

const getUrlCompleta = (url) => `${urlBase}${url}`

export function getFavorecidos() {
  return axios.get(getUrlCompleta('/favorecidos'))
}

export function cadastrarFavorecido(dados) {
  return axios.post(getUrlCompleta('/favorecidos/novo/'), dados)
}

export function deletarFavorecidos(idsParaDeletar) {
  return axios.delete(getUrlCompleta(`/favorecidos/deletar`), {
    data: idsParaDeletar,
  })
}

export function editarFavorecido(idFavorecido, dados) {
  return axios.put(getUrlCompleta(`/favorecidos/${idFavorecido}/editar`), dados)
}
