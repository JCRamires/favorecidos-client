import merge from 'lodash/merge'
import cloneDeep from 'lodash/cloneDeep'

export const schemaBanco = {
  agency: {
    maxLength: 10,
    required: true,
    pattern: /^(?!0+$)[0-9]{0,10}$/,
    digit: {
      exactLength: 1,
      required: false,
      pattern: /^[0-9]{0,1}$/,
    },
  },
  account: {
    maxLength: 11,
    required: true,
    pattern: /^(?!0+$)[0-9]{0,11}$/,
    digit: {
      exactLength: 1,
      required: true,
      pattern: /^[0-9]{0,1}$/,
    },
  },
  accountType: {
    required: true,
    allowedTypes: ['CONTA_CORRENTE', 'CONTA_POUPANCA'],
  },
}

export const schemaBancoDoBrasil = merge(cloneDeep(schemaBanco), {
  agency: {
    maxLength: 4,
    digit: {
      pattern: /^[xX0-9]{0,1}$/,
    },
  },
  account: {
    maxLength: 8,
    digit: {
      pattern: /^(?!0+$)[0-9]{0,11}$/,
    },
  },
  accountType: {
    allowedTypes: ['CONTA_CORRENTE', 'CONTA_POUPANCA', 'CONTA_FACIL'],
  },
})
