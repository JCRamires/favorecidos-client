import React from 'react'
import { Table } from 'antd'

import TagStatus from '../componentes/TagStatus'

import { formatCnpjCpf, getIconeBancoPorCodigo } from '../utils/exibicaoUtils'

// TODO: sorting data

function TabelaFavorecidos({ carregando, favorecidos, selecaoLinhas, onClickFavorecido }) {
  const colunas = [
    {
      title: 'Favorecido',
      dataIndex: 'nome',
      key: 'nome',
      sorter: true,
      render: (nome, favorecido) => (
        // eslint-disable-next-line
        <a
          href="#"
          data-testid="nome-favorecido"
          onClick={() => onClickFavorecido(favorecido['_id'])}
        >
          {nome}
        </a>
      ),
    },
    {
      title: 'CPF/CNPJ',
      dataIndex: 'documento',
      key: 'documento',
      render: (documento) => formatCnpjCpf(documento),
    },
    {
      title: 'Banco',
      dataIndex: 'banco',
      key: 'banco',
      render: (banco) => getIconeBancoPorCodigo(banco),
    },
    {
      title: 'Agência',
      dataIndex: 'agencia',
      key: 'agencia',
      render: (agencia, dadosFavorecido) =>
        dadosFavorecido.digito_agencia ? `${agencia}-${dadosFavorecido.digito_agencia}` : agencia,
    },
    {
      title: 'CC',
      dataIndex: 'conta',
      key: 'conta',
      render: (conta, dadosFavorecido) =>
        dadosFavorecido.digito_conta ? `${conta}-${dadosFavorecido.digito_conta}` : conta,
    },
    {
      title: 'Status do favorecido',
      dataIndex: 'status',
      key: 'status',
      render: (status) => <TagStatus status={status} />,
    },
  ]

  return (
    <Table
      loading={carregando}
      dataSource={favorecidos}
      columns={colunas}
      rowSelection={selecaoLinhas}
      rowKey="_id"
      locale={{ emptyText: 'Sem dados para exibir' }}
    />
  )
}

export default TabelaFavorecidos
