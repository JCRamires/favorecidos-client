import React from 'react'
import { render } from '@testing-library/react'
import TabelaFavorecidos from '../TabelaFavorecidos'

const listaFavorecidos = [
  {
    status: 1,
    deletado: false,
    _id: '1',
    nome: 'Barbara da Silva Silveira Fontes',
    documento: '02193523912',
    email: 'teste@teste.com',
    banco: '237',
    agencia: '0814',
    digito_agencia: '0',
    tipo_conta: 'CONTA_POUPANCA',
    conta: '01002713',
    digito_conta: '1',
    __v: 0,
  },
  {
    status: 2,
    deletado: false,
    _id: '2',
    nome: 'Guilherme Damian Verdasca',
    documento: '06795621928',
    email: 'teste2@teste2.com',
    banco: '001',
    agencia: '123',
    digito_agencia: 'X',
    tipo_conta: 'CONTA_POUPANCA',
    conta: '01002713',
    digito_conta: '9',
    __v: 0,
  },
  {
    status: 1,
    deletado: false,
    _id: '3',
    nome: 'Transfeera Serviços de Pagamentos Ltda',
    documento: '27084098000169',
    email: 'teste3@teste3.com',
    banco: '756',
    agencia: '1000',
    digito_agencia: '',
    tipo_conta: 'CONTA_POUPANCA',
    conta: '01002713',
    digito_conta: '9',
    __v: 0,
  },
]

test('Deve exibir mensagem correta quando não possui dados para exibir', () => {
  const { getByText } = render(<TabelaFavorecidos />)
  expect(getByText('Sem dados para exibir')).toBeInTheDocument()
})

test('Deve exibir dados corretamente', () => {
  const { getByText, getAllByTestId } = render(
    <TabelaFavorecidos favorecidos={listaFavorecidos} onClickFavorecido={jest.fn()} />
  )

  expect(getByText('Barbara da Silva Silveira Fontes')).toBeInTheDocument()
  expect(getByText('Guilherme Damian Verdasca')).toBeInTheDocument()
  expect(getByText('Transfeera Serviços de Pagamentos Ltda')).toBeInTheDocument()

  expect(getAllByTestId('nome-favorecido').length).toBe(3)
})
