import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'

import { bancos } from '../../enums'

import SelectBanco from '../SelectBanco'

const labelBancoDoBrasil = `${bancos.BANCO_DO_BRASIL.label} (${bancos.BANCO_DO_BRASIL.codigo})`

test('Deve exibir corretamente o valor selecionado', () => {
  const { getByText } = render(<SelectBanco value={bancos.BANCO_DO_BRASIL.codigo} />)

  expect(getByText(labelBancoDoBrasil)).toBeInTheDocument()
})

test('Deve enviar o codigo correto ao mudar valor do select', () => {
  const onChangeBanco = jest.fn()
  render(<SelectBanco onChange={onChangeBanco} />)

  fireEvent.mouseDown(screen.getByText('Selecione o banco'))

  fireEvent.mouseDown(screen.getByText(labelBancoDoBrasil))

  // TODO: Como clicar na opcao do select?

  // expect(onChangeBanco).toHaveBeenCalled()
})
