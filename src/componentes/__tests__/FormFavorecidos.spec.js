import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import FormFavorecidos from '../FormFavorecido'

const delay = async (timeout) => new Promise((resolve) => setTimeout(resolve, timeout))

const favorecidoTeste = {
  status: '2',
  nome: 'Pooh Napoleone',
  documento: '82268902099',
  email: 'pnapoleone0@chron.com',
  banco: '104',
  agencia: '3149',
  digito_agencia: '2',
  tipo_conta: 'CONTA_CORRENTE',
  conta: '47400',
  digito_conta: '1',
}

test('Deve chamar onSubmit com os dados corretos', async () => {
  const onSubmit = jest.fn()
  const { getByTestId } = render(<FormFavorecidos onSubmit={onSubmit} />)

  await userEvent.type(getByTestId('nome'), 'Nome teste')
  await userEvent.type(getByTestId('documento'), '1234567890')
  await userEvent.type(getByTestId('email'), 'teste@teste.com')

  await userEvent.type(screen.getAllByRole('combobox')[0], '001')
  await userEvent.click(screen.getByText('Banco do Brasil (001)'))

  await userEvent.type(getByTestId('agencia'), '1234567890')
  await userEvent.type(getByTestId('digitoAgencia'), '1')

  await userEvent.click(screen.getAllByRole('combobox')[1])
  await userEvent.click(screen.getByText('Poupança'))

  await userEvent.type(getByTestId('conta'), '12345678')
  await userEvent.type(getByTestId('digitoConta'), '2')

  await userEvent.click(screen.getByRole('button', { name: 'Salvar' }))

  await delay(500)

  expect(onSubmit).toHaveBeenCalledTimes(1)
  expect(onSubmit.mock.calls[0]).toMatchSnapshot()
})

test('Deve exibir botão de deletar quando esté em modal', async () => {
  render(<FormFavorecidos onSubmit={jest.fn()} favorecidoEditar={favorecidoTeste} onClickDelete={jest.fn()} />)

  expect(screen.getByTestId('botao-deletar')).toBeInTheDocument()
})
