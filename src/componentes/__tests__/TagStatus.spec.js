import React from 'react'
import { render, screen } from '@testing-library/react'

import TagStatus from '../TagStatus'

test('Deve exibir tag rascunho quando status é igual a 1', () => {
  render(<TagStatus status={1} />)
  expect(screen.getByText('Rascunho')).toBeInTheDocument()
})

test('Deve exibir tag validado quando status é igual a 2', () => {
  render(<TagStatus status={2} />)
  expect(screen.getByText('Validado')).toBeInTheDocument()
})