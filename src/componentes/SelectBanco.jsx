import React from 'react'
import { Select } from 'antd'

import { filtroSelectBancos } from '../utils/pesquisaUtils'

import { bancos } from '../enums'

const opcoesBanco = Object.keys(bancos).map((chave) => {
  const banco = bancos[chave]
  return { label: `${banco.label} (${banco.codigo})`, value: banco.codigo }
})

const SelectBanco = ({ value, onChange }) => (
  <Select
    style={{ width: '250px' }}
    placeholder="Selecione o banco"
    notFoundContent="Banco não encontrado"
    showSearch
    value={value}
    onChange={(value) => onChange(value)}
    options={opcoesBanco}
    filterOption={filtroSelectBancos}
  />
)

export default SelectBanco
