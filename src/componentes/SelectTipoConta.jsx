import React from 'react'
import { Select } from 'antd'

import { TIPOS_CONTA } from '../utils/exibicaoUtils'

// TODO: fazer a opcão Conta Facil funcionar no form de cadastro

const SelectBanco = ({ value, onChange, ehBancoDoBrasil }) => (
  <Select
    style={{ width: '200px' }}
    placeholder="Selecione o tipo de conta"
    value={value}
    onChange={(value) => onChange(value)}
    options={ehBancoDoBrasil ? [...TIPOS_CONTA, { label: 'Conta Fácil', value: 'CONTA_FACIL' }] : TIPOS_CONTA}
  />
)

export default SelectBanco
