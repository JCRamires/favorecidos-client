import React from 'react'
import { Tag } from 'antd'

function TagStatus({ status }) {
  if (status === 2) {
    return <Tag color="green">Validado</Tag>
  }
  
  return <Tag color="geekblue">Rascunho</Tag>
}

export default TagStatus