import React, { useState } from 'react'
import { Typography, Form, Input, Button, Row, Col } from 'antd'
import { DeleteOutlined } from '@ant-design/icons'

import {
  getRegraDoSchema,
  getRegrasAgencia,
  getRegrasDigitoAgencia,
  getRegrasConta,
  getRegrasDigitoConta,
} from '../utils/regrasValidacaoCampoUtils'
import { getNomeBancoPorCodigo, getTipoContaPorCodigo, formatCnpjCpf } from '../utils/exibicaoUtils'

import SelectBanco from './SelectBanco'
import SelectTipoConta from './SelectTipoConta'

const LabelEConteudo = ({ label, children }) => (
  <div style={{ display: 'flex', flexDirection: 'column' }}>
    <div style={{ fontSize: '16px' }}>{label}</div>
    <div style={{ fontSize: '24px' }}>{children}</div>
  </div>
)

const { Title } = Typography

function FormFavorecido({ onSubmit, onClickFechar, favorecidoEditar, onClickDelete }) {
  const [bancoDoBrasilSelecionado, setBancoDoBrasilSelecionado] = useState(false)
  const [desabilitarSubmit, setDesabilitarSubmit] = useState(false)

  const editando = favorecidoEditar !== undefined

  const onValuesChange = (changedValues, allValues) => {
    if (Object.keys(changedValues).includes('banco')) {
      if (changedValues.banco === '001') {
        setBancoDoBrasilSelecionado(true)
      } else {
        setBancoDoBrasilSelecionado(false)
      }
    }
  }

  const onFinish = (values) => {
    setDesabilitarSubmit(true)
    onSubmit(values)
  }

  return (
    <Form
      name="form-favorecido"
      onValuesChange={onValuesChange}
      onFinish={onFinish}
      initialValues={favorecidoEditar || {}}
    >
      {editando && favorecidoEditar.status === 2 ? (
        <>
          <Row>
            <Col>
              <LabelEConteudo label="CPF/CNPJ">{formatCnpjCpf(favorecidoEditar.documento)}</LabelEConteudo>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <LabelEConteudo label="Banco">
                {getNomeBancoPorCodigo(favorecidoEditar.banco)}
              </LabelEConteudo>
            </Col>
            <Col span={12}>
              <LabelEConteudo label="Agência">{`${favorecidoEditar.agencia}-${favorecidoEditar.digito_agencia}`}</LabelEConteudo>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <LabelEConteudo label="Tipo de conta">
                {getTipoContaPorCodigo(favorecidoEditar.tipo_conta)}
              </LabelEConteudo>
            </Col>
            <Col span={12}>
              <LabelEConteudo label="Conta">{`${favorecidoEditar.conta}-${favorecidoEditar.digito_conta}`}</LabelEConteudo>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <div>Qual o e-mail do favorecido?</div>
              <Form.Item
                name="email"
                validateTrigger="onBlur"
                rules={[
                  { required: true, message: 'Campo obrigatório' },
                  { type: 'email', message: 'E-mail inválido' },
                ]}
              >
                <Input data-testid="email" />
              </Form.Item>
            </Col>
          </Row>
        </>
      ) : (
        <>
          <Title level={4}>Quais os dados do favorecido?</Title>

          <Row gutter={16}>
            <Col>
              <div>Qual o nome completo ou razão social do favorecido?</div>
              <Form.Item name="nome" rules={[{ required: true, message: 'Campo obrigatório' }]}>
                <Input data-testid="nome" />
              </Form.Item>
            </Col>
            <Col>
              <div>Qual o CPF ou CNPJ?</div>
              <Form.Item
                name="documento"
                rules={[{ required: true, message: 'Campo obrigatório' }]}
              >
                <Input data-testid="documento" maxLength={14} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <div>Qual o e-mail do favorecido?</div>
              <Form.Item
                name="email"
                validateTrigger="onBlur"
                rules={[
                  { required: true, message: 'Campo obrigatório' },
                  { type: 'email', message: 'E-mail inválido' },
                ]}
              >
                <Input data-testid="email" />
              </Form.Item>
            </Col>
          </Row>

          <Title level={4}>Quais os dados bancários do favorecido?</Title>

          <Row gutter={16}>
            <Col>
              <div>Qual o banco do favorecido?</div>
              <Form.Item name="banco" rules={[{ required: true, message: 'Campo obrigatório' }]}>
                <SelectBanco />
              </Form.Item>
            </Col>
            <Col>
              <div>Qual a agência?</div>
              <Form.Item name="agencia" rules={getRegrasAgencia(bancoDoBrasilSelecionado)}>
                <Input
                  data-testid="agencia"
                  maxLength={getRegraDoSchema(bancoDoBrasilSelecionado, 'agency.maxLength')}
                />
              </Form.Item>
            </Col>
            <Col>
              <div>Dígito</div>
              <Form.Item
                name="digito_agencia"
                rules={getRegrasDigitoAgencia(bancoDoBrasilSelecionado)}
              >
                <Input data-testid="digitoAgencia" maxLength={1} />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col>
              <div>Qual o tipo da conta?</div>
              <Form.Item
                name="tipo_conta"
                rules={[{ required: true, message: 'Campo obrigatório' }]}
              >
                <SelectTipoConta />
              </Form.Item>
            </Col>
            <Col>
              <div>Qual a conta corrente?</div>
              <Form.Item name="conta" rules={getRegrasConta(bancoDoBrasilSelecionado)}>
                <Input
                  data-testid="conta"
                  maxLength={getRegraDoSchema(bancoDoBrasilSelecionado, 'account.maxLength')}
                />
              </Form.Item>
            </Col>
            <Col>
              <div>Dígito</div>
              <Form.Item name="digito_conta" rules={getRegrasDigitoConta(bancoDoBrasilSelecionado)}>
                <Input data-testid="digitoConta" maxLength={1} />
              </Form.Item>
            </Col>
          </Row>
        </>
      )}

      <Row gutter={16}>
        <Col>
          <Form.Item>
            <Button disabled={desabilitarSubmit} onClick={onClickFechar}>
              Cancelar
            </Button>
          </Form.Item>
        </Col>
        {editando && (
          <Col>
            <Form.Item>
              <Button data-testid="botao-deletar" type="danger" icon={<DeleteOutlined />} onClick={onClickDelete} />
            </Form.Item>
          </Col>
        )}
        <Col>
          <Form.Item>
            <Button type="primary" htmlType="submit" disabled={desabilitarSubmit}>
              Salvar
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}

export default FormFavorecido
