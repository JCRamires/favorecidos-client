export const bancos = {
  BRADESCO: { codigo: '237', label: 'Bradesco', icone: 'ibb-bradesco' },
  CAIXA: { codigo: '104', label: 'Caixa Econômica Federal', icone: 'ibb-caixa' },
  SICOOB: { codigo: '756', label: 'Sicoob', icone: 'ibb-sicoob' },
  BANCO_DO_BRASIL: { codigo: '001', label: 'Banco do Brasil', icone: 'ibb-banco-brasil' },
}
