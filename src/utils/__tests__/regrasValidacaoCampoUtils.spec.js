import {
  getRegrasAgencia,
  getRegrasDigitoAgencia,
  getRegrasConta,
  getRegrasDigitoConta,
} from '../regrasValidacaoCampoUtils'

describe('Banco do Brasil', () => {
  test('getRegrasAgencia', () =>
    expect(getRegrasAgencia(true)).toMatchSnapshot())

  test('getRegrasDigitoAgencia', () =>
    expect(getRegrasDigitoAgencia(true)).toMatchSnapshot())

  test('getRegrasConta', () => expect(getRegrasConta(true)).toMatchSnapshot())

  test('getRegrasDigitoConta', () =>
    expect(getRegrasDigitoConta(true)).toMatchSnapshot())
})

describe('Outros bancos', () => {
  test('getRegrasAgencia', () =>
    expect(getRegrasAgencia(false)).toMatchSnapshot())

  test('getRegrasDigitoAgencia', () =>
    expect(getRegrasDigitoAgencia(false)).toMatchSnapshot())

  test('getRegrasConta', () => expect(getRegrasConta(false)).toMatchSnapshot())

  test('getRegrasDigitoConta', () =>
    expect(getRegrasDigitoConta(false)).toMatchSnapshot())
})
