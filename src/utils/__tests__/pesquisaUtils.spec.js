import { filtroSelectBancos } from '../pesquisaUtils'

const banco = { label: 'Banco do Brasil', value: '001' }

test('Deve retornar verdadeiro quando o codigo do banco contem o valor da pesquisa', () => {
  expect(filtroSelectBancos('1', banco)).toBe(true)
})

test('Deve retornar verdadeiro quando o nome do banco contem o valor da pesquisa', () => {
  expect(filtroSelectBancos('Brasil', banco)).toBe(true)
})

test('Deve retornar verdadeiro quando o nome do banco contem o valor da pesquisa em minusculo', () => {
  expect(filtroSelectBancos('brasil', banco)).toBe(true)
})

test('Deve retornar false quando o termo não é encontrado no nome ou código', () => {
  expect(filtroSelectBancos('bradesco', banco)).toBe(false)
})
