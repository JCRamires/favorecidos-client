export function filtroSelectBancos(valorInput, opcao) {
  const nomeBanco = opcao.label.toLowerCase()

  return opcao.value.includes(valorInput) || nomeBanco.includes(valorInput.toLowerCase())
}
