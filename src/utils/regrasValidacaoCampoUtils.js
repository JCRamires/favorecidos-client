import get from 'lodash/get'

import { schemaBanco, schemaBancoDoBrasil } from '../schemasValidacao'

export const getRegraDoSchema = (ehBancoDoBrasil, caminhoNoSchema) =>
  ehBancoDoBrasil ? get(schemaBancoDoBrasil, caminhoNoSchema) : get(schemaBanco, caminhoNoSchema)

export const getRegrasAgencia = (ehBancoDoBrasil) => [
  {
    required: getRegraDoSchema(ehBancoDoBrasil, 'agency.required'),
    message: 'Campo obrigatório',
  },
  {
    max: getRegraDoSchema(ehBancoDoBrasil, 'agency.maxLength'),
    message: `Máximo de ${getRegraDoSchema(ehBancoDoBrasil, 'agency.maxLength')} caracteres`,
  },
  {
    pattern: getRegraDoSchema(ehBancoDoBrasil, 'agency.pattern'),
    message: 'Valor inválido',
  },
]

export const getRegrasDigitoAgencia = (ehBancoDoBrasil) => [
  {
    len: getRegraDoSchema(ehBancoDoBrasil, 'agency.digit.exactLength'),
    message: 'Dígito deve possuir 1 caractere',
  },
  {
    required: getRegraDoSchema(ehBancoDoBrasil, 'agency.digit.required'),
    message: 'Campo obrigatório',
  },
  {
    pattern: getRegraDoSchema(ehBancoDoBrasil, 'agency.digit.pattern'),
    message: 'Este dígito não é válido para esta agência',
  },
]

export const getRegrasConta = (ehBancoDoBrasil) => [
  {
    required: getRegraDoSchema(ehBancoDoBrasil, 'account.required'),
    message: 'Campo obrigatório',
  },
  {
    max: getRegraDoSchema(ehBancoDoBrasil, 'account.maxLength'),
    message: `Máximo de ${getRegraDoSchema(ehBancoDoBrasil, 'account.maxLength')} caracteres`,
  },
  {
    pattern: getRegraDoSchema(ehBancoDoBrasil, 'account.pattern'),
    message: 'Valor inválido',
  },
]

export const getRegrasDigitoConta = (ehBancoDoBrasil) => [
  {
    len: getRegraDoSchema(ehBancoDoBrasil, 'account.digit.exactLength'),
    message: 'Dígito deve possuir 1 caractere',
  },
  {
    required: getRegraDoSchema(ehBancoDoBrasil, 'account.digit.required'),
    message: 'Campo obrigatório',
  },
  {
    pattern: getRegraDoSchema(ehBancoDoBrasil, 'account.digit.pattern'),
    message: 'Este dígito não é válido para esta agência',
  },
]
