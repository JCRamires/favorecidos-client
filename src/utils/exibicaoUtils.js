import React from 'react'

import { bancos } from '../enums'

export const TIPOS_CONTA = [
  { label: 'Conta Corrente', value: 'CONTA_CORRENTE' },
  { label: 'Poupança', value: 'CONTA_POUPANCA' },
]

export function getNomeBancoPorCodigo(codigoBanco) {
  const bancoRetornar = Object.values(bancos).find((banco) => banco.codigo === codigoBanco)
  if (bancoRetornar !== undefined) {
    return bancoRetornar.label
  }

  return 'Banco desconhecido'
}

export function getTipoContaPorCodigo(codigoTipoConta) {
  const tipoContaRetornar = TIPOS_CONTA.find((tipoConta) => tipoConta.value === codigoTipoConta)
  if (tipoContaRetornar !== undefined) {
    return tipoContaRetornar.label
  }

  return 'Tipo de conta desconhecido'
}

export function formatCnpjCpf(value) {
  const cnpjCpf = value.replace(/\D/g, '')

  if (cnpjCpf.length === 11) {
    return cnpjCpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4')
  }

  return cnpjCpf.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '$1.$2.$3/$4-$5')
}


export function getIconeBancoPorCodigo(codigoBanco) {
  const bancoRetornar = Object.values(bancos).find((banco) => banco.codigo === codigoBanco)

  return <div style={{ fontSize: '30px' }} className={bancoRetornar.icone} />
}