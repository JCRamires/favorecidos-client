# Para rodar:

- Subir o servidor localizado [neste repo](https://gitlab.com/JCRamires/favorecidos-server)
- Clonar repo do client
- `npm install` ou `yarn install` na pasta clonada
- `npm start` ou `yarn start`